import { Component } from "react";
import D3LineChart from './Graphics/LineChart'

export default class ChartWrapper extends Component {
    componentDidMount() {
        this.setState({
            chart: new D3LineChart(this.refs.chart, this.props.data)
        })
    }

    shouldComponentUpdate() {
        return false;
    }

    componentWillReceiveProps(nextProps) {
        // this.state.chart.update(nextProps)
    }


    render() {
        return <div className="chart-area" ref="chart" ></div>
    }

}