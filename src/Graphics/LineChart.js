import * as d3 from 'd3'

const MARGIN = { TOP: 10, BOTTOM: 80, LEFT: 70, RIGHT: 10};
const WIDTH = 1200 -MARGIN.LEFT - MARGIN.RIGHT;
const HEIGHT = 800 - MARGIN.TOP - MARGIN.BOTTOM

export default class D3LineChart {
    constructor(element, data) {
        let vis = this;
        vis.data = data;

        vis.g = d3.select(element)
         .append("svg")
          .attr("width", WIDTH + MARGIN.LEFT + MARGIN.RIGHT)
          .attr("height", HEIGHT + MARGIN.TOP + MARGIN.BOTTOM)
         .append("g")
          .attr("transform", `translate(${MARGIN.LEFT}, ${MARGIN.TOP})`)

        vis.x = d3.scaleLinear()
         .range([0 , WIDTH])

        vis.y = d3.scaleLinear()
         .range([HEIGHT, 0])

        vis.xAxisGroup = vis.g.append("g")
         .attr("transform", `translate(0, ${HEIGHT})`)
        vis.yAxisGroup = vis.g.append("g")

        // Label config X-axis
        vis.g.append("text")
         .attr("x", WIDTH / 2)
         .attr("y", HEIGHT + 40)
         .attr("font-size", 20)
         .attr("text-anchor", "middle")
         .text("Vilnius_TV_Tower")

        // Label config Y-axis
         vis.g.append("text")
         .attr("x", -(HEIGHT / 2))
         .attr("y", -50)
         .attr("transform", "rotate(-90)")
         .attr("text-anchor", "middle")
         .text("Height in cm")


        vis.update()
    }

    update() {
        let vis = this; 

        let minDate = new Date(d3.min(vis.data, d => d.Laikas));
        let maxDate = new Date(d3.max(vis.data, d => d.Laikas));

        vis.x.domain([minDate , maxDate])
        vis.y.domain([0 , d3.max(vis.data, d => d.Vilnius_TV_Tower)])

        const xAxisCall = d3.axisBottom(vis.x)
        const yAxisCall = d3.axisLeft(vis.y)

        vis.xAxisGroup.call(xAxisCall)
        vis.yAxisGroup.call(yAxisCall)

        // JOIN
        const circles = vis.g.selectAll("circle")
         .data(vis.data, d => d.name)

        // EXIT
        circles.exit().remove()
        console.log(vis.x(vis.data[0].Laikas))

        // // UPDATE
        // circles
        //  .attr("cx", d => vis.x(d.Laikas))
        //  .attr("cy", d => vis.y(d.Vilnius_TV_Tower))

        // // // ENTER
        // circles.enter().append("circle")
        // .attr("cx", d => vis.x(d.Laikas))
        //  .attr("cy", d => vis.y(d.Vilnius_TV_Tower))
        //  .attr("r", 3)
        //  .attr("fill", "grey")

    }
}