import React, { Component } from "react";
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import ChartWrapper from "./ChartWrapper";
import Table from "./Table"

import { json } from 'd3';


class App extends Component {
  state = {
    data: []
  }

  componentWillMount() {
    json("https://air-pollution-9cb8f-default-rtdb.firebaseio.com/SO2.json")
      .then(data => this.setState({data}))
      .catch(error => console.log(error))
  }

  updateData = (data) => this.setState({ data })

  renderChart() {
    if (this.state.data.length === 0) {
      return "No data yet"
    }
    return <ChartWrapper data={this.state.data} />
  }

  render() {
    return (
      <div >
        <Navbar bg="light" >
          <Navbar.Brand>Scatterplotly</Navbar.Brand>
        </Navbar>
        <Container>
          <Row>
            <Col md={12} xs={12}>{this.renderChart()}</Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
